import { Component, OnInit, Input } from '@angular/core';
import { NguCarousel, NguCarouselConfig } from '@ngu/carousel';
@Component({
  selector: 'app-holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.scss'],

  styles: [`
    .star {
      font-size: 1.5rem;
      color: #b0c4de;
    } .filled {
      color: orange;
    }`]
})

export class HolidayComponent implements OnInit {
  
  @Input('backgroundGray') public backgroundGray;
  public carouselOptions: NguCarouselConfig;

  money = 300;
  currentRate = 2;
  nrate=5;
  constructor() {
     
   }

  ngOnInit(): void {
  }

}
