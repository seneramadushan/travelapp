import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NguCarouselModule } from '@ngu/carousel';
import { SharedModule } from '../../shared/shared.module';
import { HomeRoutes } from "./home.routing";


import { HomeTwoComponent } from './home-two.component';
import { HeaderComponent } from './header/header.component';
import { IntroTwoComponent } from './intro-two/intro-two.component';
import { FooterComponent } from './footer/footer.component';
import { AllSectionsComponent } from './all-sections.component';
import { WINDOW_PROVIDERS } from '../../shared/helpers/window.helper';
import { SharedMaterialModule } from 'app/shared/shared-material.module';
import { SharedDirectivesModule } from 'app/shared/directives/shared-directives.module';
import { SharedComponentsModule } from 'app/shared/components/shared-components.module';


import { SearchComponent } from './search/search.component';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatTableModule} from '@angular/material/table';
import { OverviewComponent } from './overview/overview.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { HolidayComponent } from './holiday/holiday.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserreviewsComponent } from './userreviews/userreviews.component';
import { StoriesComponent } from './stories/stories.component';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    SharedDirectivesModule,
    SharedComponentsModule,
    SharedMaterialModule,
    FlexLayoutModule,
    NguCarouselModule,
    MatSelectModule,
    MatDatepickerModule,
    MatButtonModule,
    MatTableModule,
    RouterModule.forChild(HomeRoutes)
  ],
  declarations: [
 
    OverviewComponent,
    StoriesComponent,
    UserreviewsComponent,
    DestinationsComponent,
    HolidayComponent,
    HomeTwoComponent,
    HeaderComponent, 
    IntroTwoComponent, 
    FooterComponent, 
    AllSectionsComponent,
    SearchComponent
  ],
  providers: [WINDOW_PROVIDERS]

})
export class HomeModule { }
