import { Component, OnInit, Input } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-userreviews',
  templateUrl: './userreviews.component.html',
  styleUrls: ['./userreviews.component.scss'],
  styles: [`
  .star {
    font-size: 1.5rem;
    color: #b0c4de;
  } .filled {
    color: orange;
  }`],
  providers: [NgbCarouselConfig]  
})
export class UserreviewsComponent implements OnInit {
  @Input('backgroundGray') public backgroundGray;
  public carouselOptions: NguCarouselConfig;

  currentRate = 2;
  nrate=5;

users = [
  {"image" : "../../../../assets/images/user1.jpg","desc":"Lorem ipsum dolor sit amt,consecturer adipsing alit.Anenean commodo ligula eget dolor cum sociis natoque Nulla consequent massa quis enim.Donec pede justo.penatibus et magins.","name":"John Fernanz","rate":2},
  {"image" : "../../../../assets/images/user2.jpg","desc":"dfLorem ipsum dolor sit amt,consecturer adipsing alit.Anenean commodo ligula eget dolor cum sociis natoque Nulla consequent massa quis enim.Donec pede justo.penatibus et magins.","name":"Michel Johnson","rate":3},
  {"image" : "../../../../assets/images/user3.jpg","desc":"mLorem ipsum dolor sit amt,consecturer adipsing alit.Anenean commodo ligula eget dolor cum sociis natoque Nulla consequent massa quis enim.Donec pede justo.penatibus et magins.","name":"Joseph Henzees","rate":5},
]



  constructor(config: NgbCarouselConfig) { 
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }

  ngOnInit(): void {
  }

}
