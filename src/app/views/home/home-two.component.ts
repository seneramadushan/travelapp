import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home-two',
  template: `<div class="landing">
  <app-header></app-header>
  <app-intro-two></app-intro-two>
  <app-overview></app-overview>
  <app-destinations></app-destinations>
  <app-holiday></app-holiday>
  <app-userreviews></app-userreviews>
  <app-stories></app-stories>
  
  
  <app-footer></app-footer>
  </div>`
})
export class HomeTwoComponent implements OnInit, OnDestroy {
  constructor(
  ) { }

  ngOnInit() {
  }
  ngOnDestroy() {
  }
}
