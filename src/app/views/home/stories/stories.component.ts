import { Component, OnInit, Input } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {
  @Input('backgroundGray') public backgroundGray;
  public carouselOptions: NguCarouselConfig;

  images = [
    {"img":"../../../../assets/images/sri.jpg","date":"Apr 14 2020","desc":"Live with no execuses and travel with no regrets."},
    {"img":"../../../../assets/images/thai.jpg","date":"Apr 14 2020","desc":"We travel not to excape life but for life not to escape us."},
    {"img":"../../../../assets/images/bra.jpg","date":"Apr 14 2020","desc":"People don't take trips take people."}
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
